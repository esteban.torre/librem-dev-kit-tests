#!/usr/bin/env bash
LOG_DIR="logs"
LOG_FILE="$LOG_DIR/results-$(date -Ihours).log"
mkdir -p $LOG_DIR
echo "Tests started $(date -u -Ihours)." > $LOG_FILE
echo "#uname -a" >> $LOG_FILE
uname -a >> $LOG_FILE
echo "#lsmod" >> $LOG_FILE
lsmod >> $LOG_FILE
echo "#lscpu" >> $LOG_FILE
lscpu >> $LOG_FILE
echo "#lshw" >> $LOG_FILE
lshw >> $LOG_FILE
echo "#lsblk" >> $LOG_FILE
lsblk >> $LOG_FILE
echo "#lsusb -v" >> $LOG_FILE
lsusb -v >> $LOG_FILE
echo "#lspci" >> $LOG_FILE
lspci >> $LOG_FILE
echo "#dmidecode" >> $LOG_FILE
dmidecode >> $LOG_FILE 2>&1
echo "#sysbench --num-threads=1 --test=cpu --cpu-max-prime=20000 run" >> $LOG_FILE
sysbench --num-threads=1 --test=cpu --cpu-max-prime=20000 run >> $LOG_FILE
echo "#sysbench --num-threads=4 --test=cpu --cpu-max-prime=20000 run" >> $LOG_FILE
sysbench --num-threads=4 --test=cpu --cpu-max-prime=20000 run >> $LOG_FILE
echo "#sysbench --num-threads=4 --test=memory --memory-block-size=32K --memory-total-size=42G --memory-oper=read --memory-access-mode=seq run" >> $LOG_FILE
sysbench --num-threads=4 --test=memory --memory-block-size=32K --memory-total-size=42G --memory-oper=read --memory-access-mode=seq run >> $LOG_FILE
echo "#sysbench --num-threads=4 --test=memory --memory-block-size=4K --memory-total-size=42G --memory-oper=read --memory-access-mode=rnd run" >> $LOG_FILE
sysbench --num-threads=4 --test=memory --memory-block-size=4K --memory-total-size=42G --memory-oper=read --memory-access-mode=rnd run >> $LOG_FILE
echo "#sysbench --num-threads=4 --test=memory --memory-block-size=32K --memory-total-size=42G --memory-oper=write --memory-access-mode=seq run" >> $LOG_FILE
sysbench --num-threads=4 --test=memory --memory-block-size=32K --memory-total-size=42G --memory-oper=read --memory-access-mode=seq run >> $LOG_FILE
echo "#sysbench --num-threads=4 --test=memory --memory-block-size=4K --memory-total-size=42G --memory-oper=write --memory-access-mode=rnd run" >> $LOG_FILE
sysbench --num-threads=4 --test=memory --memory-block-size=4K --memory-total-size=42G --memory-oper=read --memory-access-mode=rnd run >> $LOG_FILE
echo "#hwinfo" >> $LOG_FILE
hwinfo >> $LOG_FILE

echo "Tests finished $(date -u -Ihours)." >> $LOG_FILE
